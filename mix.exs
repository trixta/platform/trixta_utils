defmodule TrixtaUtils.MixProject do
  use Mix.Project

  def project do
    [
      app: :trixta_utils,
      version: "0.1.1",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      aliases: aliases(),
      name: "TrixtaUtils",
      docs: [main: "TrixtaUtils.SystemUtils", extras: ["README.md"]],
      dialyzer: [plt_add_deps: :transitive],
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ]
      # if you want to use espec,
      # test_coverage: [tool: ExCoveralls, test_task: "espec"]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:credo, "~> 1.5", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.1", only: [:dev, :test], runtime: false},
      {:excoveralls, "~> 0.14.0", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.21", only: [:dev, :test], runtime: false},
      {:inch_ex, "~> 2.0", only: [:dev, :test], runtime: false},
      {:poison, "~> 4.0"},
      # Not related to Trixta Flows. This is used by PhoenixChannelClient.
      {:flow, "~> 1.1"},
      # Needed for PhoenixChannelClient
      {:socket, "~> 0.3"},
      {:timex, "~> 3.7"}
    ]
  end

  def aliases do
    [
      review: ["coveralls", "dialyzer", "inch", "hex.audit", "hex.outdated", "credo --strict"]
    ]
  end

  defp description() do
    "Shared utility functions for Trixta space projects."
  end

  defp package() do
    [
      organization: "trixta",
      licenses: ["AGPL-3.0-or-later"],
      links: %{"GitLab" => "https://gitlab.com/trixta/platform/trixta_utils"}
    ]
  end
end
