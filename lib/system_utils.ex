defmodule TrixtaUtils.SystemUtils do
  def system_path() do
    case :os.type() do
      {:win32, :nt} ->
        "c:/data/"

      _ ->
        "/var/tmp/"
    end
  end

  def environment(app_name) when is_atom(app_name) do
    Atom.to_string(Application.get_env(app_name, :environment))
  end

  def trixta_space_port() do
    if Application.get_env(:trixta_space, TrixtaSpaceWeb.Endpoint) do
      apply(TrixtaSpaceWeb.Endpoint, :current_port, [])
    else
      ""
    end
  end
end
