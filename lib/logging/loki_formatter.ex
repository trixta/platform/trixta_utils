defmodule TrixtaUtils.Logging.LokiFormatter do
  def format_log(level, message, timestamp, metadata) do
    Logger.Formatter.format(
      [:metadata, " level=", :level, " ", :levelpad, :message],
      level,
      format_message(message),
      timestamp,
      metadata
    )
  end

  defp format_message(message) do
    # TODO consider not Poison decoding (and encoding prior to this) as it is now an unnecessary slow down)
    case Poison.decode(message) do
      {:ok, %{} = log_body} ->
        Enum.map(log_body, fn {key, value} ->
          "t_" <>
            key <>
            "=" <>
            String.replace(get_value(value), " ", "[WHITESPACE]")
        end)
        |> Enum.join(" ")

      _ ->
        message
    end
  end

  defp get_value(value) do
    case value do
      value when is_binary(value) ->
        value

      value when is_list(value) ->
        "[#{Enum.join(value, ",")}]"

      value when is_map(value) ->
        inspect(value)

      value when is_atom(value) ->
        Atom.to_string(value)

      _ ->
        inspect(value)
    end
  end
end
