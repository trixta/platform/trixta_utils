defmodule TrixtaUtils.Logging.ConsoleSanitiser do
  require Logger

  @moduledoc """
  When logging json maps, lists or tuples to our logger we need to purge them of any sensitive information. This module provides functionality to itterate
  through various data structures and removed any keys matching the perstribed banned words list.
  """

  def sanitize_log(input) do
    try do
      sanitize(input)
    rescue
      _ ->
        Logger.warn("Could not sanitize log json")
        input
    end
  end

  defp sanitize(input) when is_map(input) do
    # Sanitising an input map for any keywords matching the banned list.
    input
    |> Enum.reduce(input, fn {key, value}, accumulator ->
      case key do
        "password" -> accumulator |> Map.put(key, "[FILTERED]")
        _ -> accumulator |> Map.put(key, sanitize(value))
      end
    end)
  end

  defp sanitize(input) when is_list(input) do
    # Sanitising list values
    input
    |> Enum.map(fn item ->
      sanitize(item)
    end)
  end

  defp sanitize(input) when is_tuple(input) do
    # Sanitising tuples. To a list and back again.
    input
    |> Tuple.to_list()
    |> sanitize()
    |> List.to_tuple()
  end

  defp sanitize(input) when is_binary(input) do
    # Sanitising string values. TODO: regex added here yet.
    input
  end

  defp sanitize(input) do
    # If type does not match any of the above we just leave it as is. numbers, floats, etc.
    input
  end
end
