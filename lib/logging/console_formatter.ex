defmodule TrixtaUtils.Logging.ConsoleFormatter do
  @epoch_seconds 62_167_219_200

  @doc """
  Logger will call this to allow us to format our log messages as json.
  NOTE: 'message' will always be a string. Logger can't log anything that doesn't implement String.Chars .
  If we try to log anything else, the Logger internals will throw a String.Chars error before it gets to this point.
  """
  def format_log(level, message, _timestamp, metadata) do
    case Poison.decode(message) do
      {:ok, %{"cat" => _} = log_body} ->
        # This log was intended for FluentBit.
        prepare_log_json(log_body, level, metadata)

      _ ->
        # This log wasn't intended for FluentBit because it isn't valid json / doesn't have a 'cat' property.
        # Just nest the message string underneath a "message" key in a map.
        %{"message" => message} |> prepare_log_json(level, metadata)
    end
  end

  defp prepare_log_json(map_to_log, level, metadata) do
    try do
      map_to_log
      |> TrixtaUtils.Logging.ConsoleSanitiser.sanitize_log()
      |> Map.put("ts", formatted_timestamp(metadata))
      |> Map.put("level", level)
      |> Poison.encode!()
      |> Kernel.<>("\n")
    rescue
      _ ->
        "Could not format log json: #{inspect({level, map_to_log, metadata})}\n"
    end
  end

  # Returns a formatted timestamp based on whether the caller wants to override the timestamp or not.
  # It's usually overridden if we want it to match the timestamp of a particular blockchain log entry.
  defp formatted_timestamp(metadata) do
    timestamp = Keyword.get(metadata, :override_timestamp) || :os.system_time(:millisecond)

    {:ok, formatted_datetime} =
      Timex.format(
        Timex.to_datetime(
          :calendar.gregorian_seconds_to_datetime(trunc(timestamp / 1000) + @epoch_seconds)
        ),
        "%Y-%m-%dT%H:%M:%S.%L",
        :strftime
      )

    formatted_datetime
  end
end
