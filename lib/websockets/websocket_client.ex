defmodule TrixtaUtils.Websockets.WebsocketClient do
  alias TrixtaUtils.Websockets.PhoenixChannelClient

  @moduledoc """
  A websocket client that is used to bridge the gap between http clients and space websocket communication in various configurations.
  """
  require Logger

  # The PhoenixChannelClient library does not support the protocol version 2.0.0
  @vsn "1.0.0"
  @one_minute 60_000

  @doc """
  Joins the session channel, sends a login action and gets a token.
  """
  def get_token(endpoint, credentials) do
    connect("#{endpoint}?vsn=#{@vsn}")
    |> Map.merge(%{:credentials => credentials})
    |> join_anon()
    |> anon_login()
  end

  def send_action_to_space(
        %{"topic" => topic, "event" => event, "payload" => payload},
        space_endpoint,
        %{"identity" => _id, "password" => _pwd} = credentials,
        options \\ %{}
      )
      when is_binary(space_endpoint) do
    send_action_to_space(space_endpoint, topic, event, payload, credentials, options)
  end

  def send_anon_action_to_space(endpoint, topic, event, payload, options \\ %{}) do
    connect("#{endpoint}?vsn=#{@vsn}")
    |> Map.merge(%{
      :topic => topic,
      :payload => payload,
      :event => event,
      :options => options
    })
    |> send_action_to_space()
  end

  def send_action_to_space(endpoint, topic, event, payload, credentials, options \\ %{})

  @doc """
  Sends an action into the space hosted by this server
  """
  def send_action_to_space(endpoint, topic, event, payload, %{"identity" => _identity, "password" => _password} = credentials, options) do
    case get_token(endpoint, credentials) do
      {:ok, token} when is_binary(token) ->
        connect("#{endpoint}?token=#{token}&vsn=#{@vsn}&agent_id=#{credentials["identity"]}")
        |> Map.merge(%{
          :topic => topic,
          :payload => payload,
          :credentials => credentials,
          :event => event,
          :options => options
        })
        |> send_action_to_space()

      {:error, error_details} ->
        %{:status => :error, :error => error_details}
    end
  end

  def send_action_to_space(endpoint, topic, event, payload, %{"identity" => _identity, "token" => _token} = credentials, options) do
    connect("#{endpoint}?token=#{credentials["token"]}&vsn=#{@vsn}&agent_id=#{credentials["identity"]}")
        |> Map.merge(%{
          :topic => topic,
          :payload => payload,
          :credentials => credentials,
          :event => event,
          :options => options
        })
        |> send_action_to_space()
  end

  def send_action_to_space(endpoint, topic, event, payload, %{"identity" => _identity} = credentials, options) do
    connect("#{endpoint}?vsn=#{@vsn}")
        |> Map.merge(%{
          :topic => topic,
          :payload => payload,
          :credentials => credentials,
          :event => event,
          :options => options
        })
        |> send_action_to_space()
  end

  @doc """
  Sends an action into the space hosted by this server
  """
  def send_action_to_space(
        %{:status => :ok, :socket => _socket, :topic => _topic, :event => _event} = state
      ) do
    state
    |> join_topic()
    |> send_event()
    |> close()
  end

  defp join_anon(%{:socket => _socket} = state) do
    join_topic(state, "space:everyone_anon")
  end

  defp anon_login(%{:status => :ok, :channel => _channel, :credentials => credentials} = state) do
    case send_event(state |> Map.put(:event, "login") |> Map.put(:payload, credentials))
         |> close() do
      %{:status => :ok, :result => result} -> {:ok, result["jwt"]}
      %{:status => :error, :result => error_details} -> {:error, error_details}
    end
  end

  defp join_topic(%{:status => :ok, :socket => _socket, :topic => topic} = state) do
    join_topic(state, topic)
  end

  defp join_topic(%{:status => :ok, :socket => socket} = state, topic_to_join) do
    channel = PhoenixChannelClient.channel(socket, topic_to_join, %{})

    case PhoenixChannelClient.join(channel, @one_minute) do
      {:ok, _} ->
        Map.put(state, :channel, channel)

      {:error, %{"reason" => reason, "detail" => detail} = reason_and_detail} ->
        Map.put(state, :status, :error) |> Map.put(:error, reason_and_detail)

      {:error, %{"reason" => reason}} ->
        Map.put(state, :status, :error) |> Map.put(:error, reason)
    end
  end

  # Pushes an action into this space and does NOT wait for the reply. Returns immediately.
  defp send_event(
         %{
           :status => :ok,
           :event => event,
           :payload => payload,
           :channel => channel,
           :options => %{:fire_and_forget => true}
         } = state
       ) do
    case PhoenixChannelClient.push(channel, event, payload) do
      :ok ->
        # The push succeeded. That's all we know and care about at this point. Return immediately. The action handler may still fail later.
        state |> Map.put(:status, :ok) |> Map.put(:result, :action_delivered)

      {:error, details} = _error ->
        Logger.error(
          "Could not push fire-and-forget action into space. Action: #{inspect(event)} . Error: #{
            inspect(details)
          }"
        )

        state |> Map.put(:status, :error) |> Map.put(:result, :action_not_delivered)
    end
  end

  # Sends an action to this space and waits for the Phoenix reply before returning.
  defp send_event(
         %{:status => :ok, :event => event, :payload => payload, :channel => channel} = state
       ) do
    # We set the timeout to :infinity because we don't want the socket to close while the flow is still running,
    # because that would kill the channel process, taking the linked flow handler process with it.
    # We should handle timeouts at the HTTP level.
    case PhoenixChannelClient.push_and_receive(channel, event, payload, :infinity) do
      {"phx_reply", response} ->
        state |> Map.put(:result, response)

      {:ok, response} ->
        state |> Map.put(:result, response)

      {:error, error_details} ->
        state |> Map.put(:status, :error) |> Map.put(:result, error_details)

      unknown_response ->
        Logger.error(
          "WebsocketClient: Could not send event #{inspect(event)} into channel with topic #{
            inspect(channel.topic)
          } . Details: #{inspect(unknown_response)}"
        )

        state |> Map.put(:status, :error) |> Map.put(:result, unknown_response)
        # TODO: Might want to handle other error cases differently
    end
  end

  defp send_event(state) do
    # Something went wrong earlier in the process and we can't contunue sending the event.
    state
  end

  def connect(url) do
    :crypto.start()
    :ssl.start()
    uri = URI.parse(url)
    {:ok, client_pid} = PhoenixChannelClient.start_link()

    case PhoenixChannelClient.connect(client_pid,
           host: uri.host,
           path: uri.path,
           port: uri.port,
           params:
             if is_binary(uri.query) do
               URI.decode_query(uri.query)
             else
               %{}
             end,
           secure: uri.scheme === "wss",
           heartbeat_interval: 30_000
         ) do
      {:ok, socket} ->
        %{:status => :ok, :client_pid => client_pid, :socket => socket}

      {:error, error_details} = result ->
        Logger.error(
          "WebsocketClient could not open connection to socket url: #{url} . Error details: #{
            inspect(error_details)
          }"
        )

        result
    end
  end

  @doc """
  Leaves the channel.
  """
  def close(%{:channel => channel} = state) do
    PhoenixChannelClient.leave(channel)
    stop_client_genserver(state)
    Map.delete(state, :channel)
  end

  def close(state) do
    # We never had a channel because something went wrong earlier in the process.
    # No channel to leave, but we might still have to stop the PhoenixChannelClient GenServer
    # that was started earlier:
    stop_client_genserver(state)
    state
  end

  defp stop_client_genserver(%{:client_pid => pid} = state) when is_pid(pid) do
    GenServer.stop(pid)
    state
  end

  defp stop_client_genserver(state) do
    # No GenServer pid to stop. Do nothing.
    state
  end
end
