defmodule TrixtaUtils.Serialisation.RoleMockDeserialiser do
  @moduledoc """
  When defining flow integration tests, some mock input -> output mappings can be passed in for RequestForResponse reactions, etc.
  The input / output formats are specified in the Elixir step modules, which may not be serializable to JSON.
  This module deserialises json from a format we decided on, so that the original Elixir terms can be received over the wore as json when running the test.

  NOTE: This approach assumed that all parties understand various Elixir terms, e.g. atoms, tuples, etc. It doesn't attempt to generate language-agnostic json.
  This currently only supports those terms that are common within role mocks. It needs more testing to be useful as a general serialisation protocol.
  """

  def from_json({:ok, json}) do
    from_json(json)
  end

  def from_json(json) when is_binary(json) do
    case Poison.decode(json) do
      {:ok, decoded} ->
        {:ok, recover_original(decoded)}

      _ ->
        raise ArgumentError,
              "Could not decode json when converting to role mock Elixir terms. Received json: #{
                json
              }"
    end
  end

  def from_json(json) do
    raise ArgumentError,
          "Could not decode json when converting to role mock Elixir terms. Received json: #{json}"
  end

  def recover_original(["<tuple>" | tuple_elements]) do
    # This was originally supposed to be a tuple. Convert it back to a tuple after converting the individual items:
    tuple_elements |> recover_original() |> List.to_tuple()
  end

  def recover_original(list) when is_list(list) do
    # This is a regular list, i.e. not a tuple originally. Just convert all the elements.
    list |> Enum.map(&recover_original/1)
  end

  def recover_original(":" <> atom_name) do
    # This was originally an atom. Convert it back.
    String.to_existing_atom(atom_name)
  end

  def recover_original(map) when is_map(map) do
    map
    |> Enum.reduce(%{}, fn {key, value}, acc ->
      acc |> Map.put(recover_original(key), recover_original(value))
    end)
  end

  def recover_original(value) do
    # Nothing special going on with this value.
    value
  end
end
