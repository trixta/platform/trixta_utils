defmodule TrixtaUtils.Serialisation.RoleMockSerialiser do
  @moduledoc """
  When defining flow integration tests, some mock input -> output mappings can be passed in for RequestForResponse reactions, etc.
  The input / output formats are specified in the Elixir step modules, which may not be serializable to JSON.
  This module serialises these values to a format we decided on, so that they can be passed over the wire for defining the test.

  NOTE: This approach assumed that all parties understand various Elixir terms, e.g. atoms, tuples, etc. It doesn't attempt to generate language-agnostic json.
  This currently only supports those terms that are common within role mocks. It needs more testing to be useful as a general serialisation protocol.
  """

  def to_json(value) do
    value |> make_serialisable() |> Poison.encode()
  end

  def make_serialisable(atom) when is_atom(atom) do
    # Atoms are prefixed with ":" by convention.
    ":" <> Atom.to_string(atom)
  end

  def make_serialisable(tuple) when is_tuple(tuple) do
    # For tuples, we serialise to a list with '<tuple>' as the first element.
    converted_elements =
      Tuple.to_list(tuple)
      |> Enum.map(fn el ->
        make_serialisable(el)
      end)

    ["<tuple>"] ++ converted_elements
  end

  def make_serialisable(list) when is_list(list) do
    list |> Enum.map(&make_serialisable/1)
  end

  def make_serialisable(map) when is_map(map) do
    map
    |> Enum.reduce(%{}, fn {key, val}, acc ->
      acc |> Map.put(make_serialisable(key), make_serialisable(val))
    end)
  end

  def make_serialisable(value) do
    # Everything else is kept as-is.
    value
  end
end
