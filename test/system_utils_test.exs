defmodule TrixtaUtils.SystemUtilsTest do
  use ExUnit.Case
  doctest TrixtaUtils.SystemUtils

  test "can get the system path" do
    assert TrixtaUtils.SystemUtils.system_path() in ["/var/tmp/", "c:/data"]
  end
end
