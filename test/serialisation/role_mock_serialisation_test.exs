defmodule TrixtaUtils.Serialisation.RoleMockSerialisationTest do
  use ExUnit.Case
  doctest TrixtaUtils.SystemUtils
  alias TrixtaUtils.Serialisation.RoleMockSerialiser
  alias TrixtaUtils.Serialisation.RoleMockDeserialiser

  test "can serialise common test mocks" do
    mock_to_serialise = {
      [:ok, %{:current_agent_id => "tester@example.com"}],
      [
        "success",
        [
          %{"name" => "Alert 1", "query" => "{\"query\": {}}"},
          %{"name" => "Alert 2", "query" => "{}"}
        ]
      ]
    }

    {:ok, json} = RoleMockSerialiser.to_json(mock_to_serialise)

    # The tuple should have become a list with '<tuple>' as the first element.
    # The atoms should have become strings prefixed with ':'
    assert json ===
             "[\"<tuple>\",[\":ok\",{\":current_agent_id\":\"tester@example.com\"}],[\"success\",[{\"query\":\"{\\\"query\\\": {}}\",\"name\":\"Alert 1\"},{\"query\":\"{}\",\"name\":\"Alert 2\"}]]]"
  end

  test "can deserialise common test mocks" do
    json =
      "[\"<tuple>\",[\":ok\",{\":current_agent_id\":\"tester@example.com\"}],[\"success\",[{\"query\":\"{\\\"query\\\": {}}\",\"name\":\"Alert 1\"},{\"query\":\"{}\",\"name\":\"Alert 2\"}]]]"

    {:ok, deserialised} = RoleMockDeserialiser.from_json(json)

    # We should get our original mock definition back, with tuple and atoms intact.
    assert deserialised === {
             [:ok, %{:current_agent_id => "tester@example.com"}],
             [
               "success",
               [
                 %{"name" => "Alert 1", "query" => "{\"query\": {}}"},
                 %{"name" => "Alert 2", "query" => "{}"}
               ]
             ]
           }
  end

  test "keyword lists are converted properly" do
    original_list = [key1: "value1", key1: "value2", key3: :value3]

    {:ok, deserialised_list} =
      original_list |> RoleMockSerialiser.to_json() |> RoleMockDeserialiser.from_json()

    assert deserialised_list === original_list
  end
end
